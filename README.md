# README #

Here is a sample read me file for the project.  Prof. Patrick H. Madden made this.  And to make grading easy, include a snippet of your git log.

----
[Nostromo-2:cs580/phm/helloworld] pmadden% git log
commit 647b02ae03b2509c34c04612df07410d9d842c04
Author: Patrick Madden <profmadden@bitbucket.org>
Date:   Thu Jan 28 19:24:47 2016 -0500

    I made some changes as an example

commit 8690b39f172ecdd0dc46fe1e98019f22c3eaf652
Author: Patrick Madden <profmadden@bitbucket.org>
Date:   Tue Jan 26 08:28:40 2016 -0500

    Made the changes required to switch the message to my name.

commit 9239543b5b6eecdbe4e6d14a7669ffe961086d0a
Author: Patrick Madden <profmadden@bitbucket.org>
Date:   Tue Jan 26 08:21:04 2016 -0500

    Added in the automagically generated Xcode project

commit 53bfb1a38b1ca0c264e07d1b785d3b488c493c74
Author: Patrick Madden <pmadden@acm.org>
Date:   Tue Jan 26 08:10:14 2016 -0500

    Initial commit
----