//
//  ViewController.m
//  Hello World
//
//  Created by Patrick Madden on 1/26/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked:(id)sender
{
    [helloLabel setText:@"Patrick Madden completed the first assignment."];
}

@end
