//
//  ViewController.h
//  Hello World
//
//  Created by Patrick Madden on 1/26/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *helloLabel;

-(IBAction)clicked:(id)sender;

@end

